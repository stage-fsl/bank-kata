from src.exception import NegativeValueError


class Account:
    def __init__(self, initial_amount=0):
        self.balance = initial_amount

    def get_balance(self):
        return self.balance

    def deposit(self, deposit_amount):
        if deposit_amount < 0:
            raise NegativeValueError(deposit_amount)
        else:
            self.balance += deposit_amount
