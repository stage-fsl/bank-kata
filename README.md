# bank-kata

Ecrire une classe Account qui offre ces différents services :

  1. Savoir la quantité d'argent dont on dispose sur le compte (Balance)
  2. Pouvoir ajouter (deposit) ou retirer (withdraw) de l'argent du compte
  3. Pouvoir visualiser l'ensemble des transactions effectuées sur le compte