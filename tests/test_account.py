from unittest import TestCase
from src.account import Account
from src.exception import NegativeValueError


class Test(TestCase):

    def test_check_account_is_zero_when_an_empty_account_is_created(self):
        # Given
        account = Account()

        # when
        balance = account.get_balance()

        # Then
        empty_account_value = 0
        self.assertEqual(empty_account_value, balance)

    def test_deposit_add_money_to_account(self):
        # Given
        account = Account()

        # When
        account.deposit(50)

        # Then
        self.assertEqual(50, account.get_balance())

    def test_initial_balance_is_equal_to_intial_amount_entered_when_account_is_created(self):
        # Given
        account = Account(10)

        # When
        balance = account.get_balance()

        # Then
        self.assertEqual(10, balance)

    def test_deposit_returns_error_when_amount_is_negative(self):
        # Given
        account = Account()

        # When
        self.assertRaises(NegativeValueError, account.deposit, -1)
